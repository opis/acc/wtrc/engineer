#Very stupid script to set limits through CS_Studio
#Add caput and remove = ; for shell-script
#Ex: caput TS2-010CDL:Cryo-TE-82305.P_Limit_HIHI 300

#Temperature Limits

# Cernox in helium supply branch line in Jumper Connection - TT05 in CM-P&ID
TS2-010CDL:Cryo-TE-82305.P_Limit_HIHI = 300;	
TS2-010CDL:Cryo-TE-82305.P_Limit_HI = 300;
TS2-010CDL:Cryo-TE-82305.P_Limit_LO = 4;
TS2-010CDL:Cryo-TE-82305.P_Limit_LOLO = 4;

# Cernox in VLP branch line in Jumper Connection - TT06 in CM-P&ID
TS2-010CDL:Cryo-TE-82306.P_Limit_HIHI = 300;
TS2-010CDL:Cryo-TE-82306.P_Limit_HI = 300;
TS2-010CDL:Cryo-TE-82306.P_Limit_LO = 4;
TS2-010CDL:Cryo-TE-82306.P_Limit_LOLO = 4;
	
# Cernox in helium supply line at the interface to the cold box
TS2-010CDL:Cryo-TE-82311.P_Limit_HIHI = 300;
TS2-010CDL:Cryo-TE-82311.P_Limit_HI = 300;
TS2-010CDL:Cryo-TE-82311.P_Limit_LO = 4;
TS2-010CDL:Cryo-TE-82311.P_Limit_LOLO = 4;

# Cernox in VLP line at the interface to the cold box	
TS2-010CDL:Cryo-TE-82312.P_Limit_HIHI = 300;
TS2-010CDL:Cryo-TE-82312.P_Limit_HI = 300;
TS2-010CDL:Cryo-TE-82312.P_Limit_LO = 4;	
TS2-010CDL:Cryo-TE-82312.P_Limit_LOLO = 4;

# PT1000 in TS supply line at the interface to the cold box
TS2-010CDL:Cryo-TE-82313.P_Limit_HIHI = 300;
TS2-010CDL:Cryo-TE-82313.P_Limit_HI = 300;
TS2-010CDL:Cryo-TE-82313.P_Limit_LO = 4;
TS2-010CDL:Cryo-TE-82313.P_Limit_LOLO = 4;

# PT1000 in TS return line at the interface to the cold box
TS2-010CDL:Cryo-TE-82314.P_Limit_HIHI = 300;
TS2-010CDL:Cryo-TE-82314.P_Limit_HI = 300;
TS2-010CDL:Cryo-TE-82314.P_Limit_LO = 4;
TS2-010CDL:Cryo-TE-82314.P_Limit_LOLO = 4;

# PT1000 in TS supply branch line in Jumper Connection - TT60 in CM-P&ID	
TS2-010CDL:Cryo-TE-82360.P_Limit_HIHI = 300;
TS2-010CDL:Cryo-TE-82360.P_Limit_HI = 300;
TS2-010CDL:Cryo-TE-82360.P_Limit_LO = 4;
TS2-010CDL:Cryo-TE-82360.P_Limit_LOLO = 4;

# PT1000 in TS return branch line in Jumper Connection - TT65 in CM-P&ID	
TS2-010CDL:Cryo-TE-82365.P_Limit_HIHI = 300;
TS2-010CDL:Cryo-TE-82365.P_Limit_HI = 300;
TS2-010CDL:Cryo-TE-82365.P_Limit_LO = 4;
TS2-010CDL:Cryo-TE-82365.P_Limit_LOLO = 4;

# Cernox in helium manifold supply, jumper opposide side	
TS2-010CRM:Cryo-TE-001.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-001.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-001.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-001.P_Limit_LOLO = 4;	

# Cernox in helium manifold supply, jumper opposide side
TS2-010CRM:Cryo-TE-002.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-002.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-002.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-002.P_Limit_LOLO = 4;

# Cernox in helium manifold supply, jumper opposide side
TS2-010CRM:Cryo-TE-003.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-003.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-003.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-003.P_Limit_LOLO = 4;

# CERNOX in Diphasic line	
TS2-010CRM:Cryo-TE-007.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-007.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-007.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-007.P_Limit_LOLO = 4;
	
# CERNOX in Diphasic line
TS2-010CRM:Cryo-TE-008.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-008.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-008.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-008.P_Limit_LOLO = 4;

# CERNOX in Diphasic line
TS2-010CRM:Cryo-TE-009.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-009.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-009.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-009.P_Limit_LOLO = 4;

# Cernox in helium manifold to he tank, cavity 1
TS2-010CRM:Cryo-TE-010.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-010.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-010.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-010.P_Limit_LOLO = 4;

# PT100 in Power Coupler, cavity 1 - ECCTD only
TS2-010CRM:Cryo-TE-011.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-011.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-011.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-011.P_Limit_LOLO = 4;

# Cernox in Power Coupler doble wall cooling supply, cavity 1
TS2-010CRM:Cryo-TE-012.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-012.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-012.P_Limit_LO = 4;		
TS2-010CRM:Cryo-TE-012.P_Limit_LOLO = 4;	

# PT100 in Power Coupler, double wall cooling return, cavity 1
TS2-010CRM:Cryo-TE-013.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-013.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-013.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-013.P_Limit_LOLO = 4;

# PT100 in Power Coupler, double wall cooling window coupler side, cavity 1
TS2-010CRM:Cryo-TE-014.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-014.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-014.P_Limit_LO = 4;	
TS2-010CRM:Cryo-TE-014.P_Limit_LOLO = 4;	

# PT100 in Power Coupler, double wall cooling return, cavity 1
TS2-010CRM:Cryo-TE-015.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-015.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-015.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-015.P_Limit_LOLO = 4;

# Cernox in cold tuning system, cavity 1
TS2-010CRM:Cryo-TE-018.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-018.P_Limit_HI = 300;			
TS2-010CRM:Cryo-TE-018.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-018.P_Limit_LOLO = 4;

# Cernox in helium tank bottom, cavity 1
TS2-010CRM:Cryo-TE-019.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-019.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-019.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-019.P_Limit_LOLO = 4;

# Cernox in helium manifold to he tank, cavity 2	
TS2-010CRM:Cryo-TE-020.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-020.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-020.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-020.P_Limit_LOLO = 4;	

# PT100 in Power Coupler, cavity 2 - ECCTD only
TS2-010CRM:Cryo-TE-021.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-021.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-021.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-021.P_Limit_LOLO = 4;

# Cernox in Power Coupler doble wall cooling supply, cavity 2 - ECCTD only
TS2-010CRM:Cryo-TE-022.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-022.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-022.P_Limit_LO = 4;	
TS2-010CRM:Cryo-TE-022.P_Limit_LOLO = 4;

# PT100 in Power Coupler, double wall cooling return, cavity 2
TS2-010CRM:Cryo-TE-023.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-023.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-023.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-023.P_Limit_LOLO = 4;

# PT100 in Power Coupler, double wall cooling window coupler side, cavity 2
TS2-010CRM:Cryo-TE-024.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-024.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-024.P_Limit_LO = 4;	
TS2-010CRM:Cryo-TE-024.P_Limit_LOLO = 4;

# PT100 in Power Coupler, double wall cooling return, cavity 2
TS2-010CRM:Cryo-TE-025.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-025.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-025.P_Limit_LO = 4;	
TS2-010CRM:Cryo-TE-025.P_Limit_LOLO = 4;

# Cernox in cold tuning system, cavity 2 - ECCTD only
TS2-010CRM:Cryo-TE-028.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-028.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-028.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-028.P_Limit_LOLO = 4;

# Cernox in helium tank bottom, cavity 2
TS2-010CRM:Cryo-TE-029.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-029.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-029.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-029.P_Limit_LOLO = 4;

# Cernox in helium manifold to he tank, cavity 3
TS2-010CRM:Cryo-TE-030.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-030.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-030.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-030.P_Limit_LOLO = 4;

# PT100 in Power Coupler, cavity 3 - ECCTD only
TS2-010CRM:Cryo-TE-031.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-031.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-031.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-031.P_Limit_LOLO = 4;

# Cernox in Power Coupler doble wall cooling supply, cavity 3 - ECCTD only
TS2-010CRM:Cryo-TE-032.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-032.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-032.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-032.P_Limit_LOLO = 4;

# PT100 in Power Coupler, double wall cooling return, cavity 3
TS2-010CRM:Cryo-TE-033.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-033.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-033.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-033.P_Limit_LOLO = 4;

# PT100 in Power Coupler, double wall cooling window, coupler side cavity 3
TS2-010CRM:Cryo-TE-034.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-034.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-034.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-034.P_Limit_LOLO = 4;

# PT100 in Power Coupler, double wall cooling return, cavity 3
TS2-010CRM:Cryo-TE-035.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-035.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-035.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-035.P_Limit_LOLO = 4;

# Cernox in cold tuning system, cavity 3 - ECCTD only
TS2-010CRM:Cryo-TE-038.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-038.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-038.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-038.P_Limit_LOLO = 4;

# Cernox in helium tank bottom, cavity 3
TS2-010CRM:Cryo-TE-039.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-039.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-039.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-039.P_Limit_LOLO = 4;

# PT100 in Power Coupler, cavity 4 - ECCTD only
TS2-010CRM:Cryo-TE-041.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-041.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-041.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-041.P_Limit_LOLO = 4;

# Cernox in Power Coupler doble wall cooling supply, cavity 4 - ECCTD only
TS2-010CRM:Cryo-TE-042.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-042.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-042.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-042.P_Limit_LOLO = 4;

# PT100 in Power Coupler, double wall cooling return, cavity 4
TS2-010CRM:Cryo-TE-043.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-043.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-043.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-043.P_Limit_LOLO = 4;

# PT100 in Power Coupler, double wall cooling window, coupler side cavity 4
TS2-010CRM:Cryo-TE-044.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-044.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-044.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-044

# PT100 in Power Coupler, double wall cooling return, cavity 4
TS2-010CRM:Cryo-TE-045.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-045.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-045.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-045.P_Limit_LOLO = 4;

# Cernox in cold tuning system, cavity 4 - ECCTD only
TS2-010CRM:Cryo-TE-048.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-048.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-048.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-048.P_Limit_LOLO = 4;

# Cernox in helium tank bottom, cavity 4
TS2-010CRM:Cryo-TE-049.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-049.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-049.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-049.P_Limit_LOLO = 4;

# PT100 in space frame jumper opposite side
TS2-010CRM:Cryo-TE-050.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-050.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-050.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-050.P_Limit_LOLO = 4;

# PT100 in space frame jumper opposite side - ECCTD only
TS2-010CRM:Cryo-TE-051.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-051.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-051.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-051.P_Limit_LOLO = 4;

# PT100 in space frame jumper opposite side - ECCTD only
TS2-010CRM:Cryo-TE-052.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-052.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-052.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-052.P_Limit_LOLO = 4;

# PT100 in space frame jumper side - ECCTD only
TS2-010CRM:Cryo-TE-053.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-053.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-053.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-053.P_Limit_LOLO = 4;

# PT100 in space frame jumper side - ECCTD only
TS2-010CRM:Cryo-TE-054.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-054.P_Limit_HI = 300;		
TS2-010CRM:Cryo-TE-054.P_Limit_LO = 4;	
TS2-010CRM:Cryo-TE-054	

# PT100 in space frame jumper side - ECCTD only
TS2-010CRM:Cryo-TE-055.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-055.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-055.P_Limit_LO = 4;	
TS2-010CRM:Cryo-TE-055.P_Limit_LOLO = 4;	

# Cernox in magnet shield jumper side - ECCTD only
TS2-010CRM:Cryo-TE-056.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-056.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-056.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-056.P_Limit_LOLO = 4;

# Cernox in magnet shield jumper side - ECCTD only
TS2-010CRM:Cryo-TE-057.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-057.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-057.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-057.P_Limit_LOLO = 4;

# PT100 in Thermal shield cooling supply jumper side - ECCTD only
TS2-010CRM:Cryo-TE-061.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-061.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-061.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-061.P_Limit_LOLO = 4;

# PT100 in Thermal shield cooling return jumper opposite side - ECCTD only
TS2-010CRM:Cryo-TE-062.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-062.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-062.P_Limit_LO = 4;	
TS2-010CRM:Cryo-TE-062.P_Limit_LOLO = 4;	

# PT100 in Thermal shield cooling return jumper opposite side - ECCTD only
TS2-010CRM:Cryo-TE-063.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-063.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-063.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-063.P_Limit_LOLO = 4;

# PT100 in Thermal shield cooling return
TS2-010CRM:Cryo-TE-064.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-064.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-064.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-064.P_Limit_LOLO = 4;

# PT100 in Thermal shield cooling return jumper side - ECCTD only
TS2-010CRM:Cryo-TE-066.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-066.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-066.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-066.P_Limit_LOLO = 4;

# PT100 in Thermal shield cooling return jumper opposite side - ECCTD only
TS2-010CRM:Cryo-TE-067.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-067.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-067.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-067.P_Limit_LOLO = 4;

# PT100 in Thermal shield cooling thermalization return - ECCTD only
TS2-010CRM:Cryo-TE-068.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-068.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-068.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-068.P_Limit_LOLO = 4;

# PT100 in Thermal shield cooling thermalization return
TS2-010CRM:Cryo-TE-069.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-069.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-069.P_Limit_LO = 4;	
TS2-010CRM:Cryo-TE-069.P_Limit_LOLO = 4;

# Cernox in jumper connection subcooler supply
TS2-010CRM:Cryo-TE-091.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-091.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-091.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-091.P_Limit_LOLO = 4;

# Cernox in jumper connection subcooler supply
TS2-010CRM:Cryo-TE-092.P_Limit_HIHI = 300;	
TS2-010CRM:Cryo-TE-092.P_Limit_HI = 300;
TS2-010CRM:Cryo-TE-092.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-092.P_Limit_LOLO = 4;

# Cernox in jumper connection subcooler return
TS2-010CRM:Cryo-TE-093.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-093.P_Limit_HI = 300;	
TS2-010CRM:Cryo-TE-093.P_Limit_LO = 4;
TS2-010CRM:Cryo-TE-093.P_Limit_LOLO = 4;

# Cernox in jumper connection subcooler return
TS2-010CRM:Cryo-TE-094.P_Limit_HIHI = 300;
TS2-010CRM:Cryo-TE-094.P_Limit_HI = 300;		
TS2-010CRM:Cryo-TE-094.P_Limit_LO = 4;	
TS2-010CRM:Cryo-TE-094.P_Limit_LOLO = 4;	



