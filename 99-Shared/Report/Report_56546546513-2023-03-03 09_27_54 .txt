Report for Instrumentation Tests in Waterflush Manifolds

Instrumentation Check List Without Water
Date and time: 2023-03-03 09:27:54
Manifold Serial Number: 56546546513

Tests purpose:
    Verify if all sensors (analog and digital) are working
    Verify if all actuators (digital) are working

Tests Results:
    Flow Meters Alarms Test: False
    Valve Position Indicators Test: False
    Temperature Sensors Test: False
    Pressure Sensores Test: False
    Solenoid Valves Open Close Test: False

*****************************************************************************************************
** Tested following the procedures from ESS-4913233 VPL ICS HWI SAT - Electrical Verification Plan **
** and ESS-4913233 VPL ICS HWI SAT - Instrumentation Verification Check List Without Water.        **
** This report does not replace the need to complete the instrumentation checklist                 **
*****************************************************************************************************
